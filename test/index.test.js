import test from 'ava';
import { Nuxt, Builder } from 'nuxt';
import { resolve } from 'path';

let nuxt = null;

test.before('Init nuxt.js', async t => {
  const rootDir = resolve(__dirname, '..');
  let config = {};
  try {
    config = require(resolve(rootDir, 'nuxt.config.js'));
  } catch (e) {}
  config.rootDir = rootDir;
  config.dev = false;
  nuxt = new Nuxt(config);
  await new Builder(nuxt).build();
  nuxt.listen(3000, 'localhost');
});

async function titleIsCorrect(t, path, expected) {
  const window = await nuxt.renderAndGetWindow(`http://localhost:3000${path}`);
  const element = window.document.querySelector('h1');
  t.not(element, null);
  t.is(element.textContent, expected);
}
titleIsCorrect.title = (providedTitle, input, expected) =>
  `Page '${input}' has title '${expected}}'`;
test(titleIsCorrect, '/', 'Blog');
test(titleIsCorrect, '/about', 'About');

test.after('Stop server', t => {
  nuxt.close();
});
