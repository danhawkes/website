const hljs = require('highlight.js');

let host = process.env.HOST || 'localhost';
let port = process.env.PORT || 3000;

module.exports = {
  content: [
    [
      'blogposts',
      {
        page: '/thisisnotused/_slug',
        permalink: ':slug',
        generate: ['get', 'getAll'],
        isPost: true,
        anchorLevel: 3
      }
    ],
    [
      'software',
      {
        page: '/thisisnotused2/_slug',
        permalink: ':slug',
        generate: ['getAll'],
        isPost: true
      }
    ]
  ],
  parsers: {
    md: {
      extend(config) {
        config.highlight = (str, lang) => {
          if (lang && hljs.getLanguage(lang)) {
            try {
              return hljs.highlight(lang, str).value;
            } catch (__) {}
          }
          return '';
        };
      }
    }
  },
  api: {
    baseURL: `http://${host}:${port}`,
    browserBaseURL: '/..'
  }
};
