// Add cache header to responses
module.exports = function(req, res, next) {
  let url = require('url').parse(req.url);
  if (url.pathname.search(/\/content-api/) >= 0) {
    // Cache content API
    res.setHeader('Cache-Control', 'public, max-age=3600');
  } else if (url.pathname.search(/.*\..*/) === -1) {
    // Cache paths without extensions (pages)
    res.setHeader('Cache-Control', 'public, max-age=1800');
  }
  next();
};
