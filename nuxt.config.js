const cache = process.env.CACHE_ENABLE !== undefined;

let responsiveLoaderAdapter;
try {
  require.resolve('sharp');
  responsiveLoaderAdapter = require('responsive-loader/sharp');
} catch (e) {
  responsiveLoaderAdapter = require('responsive-loader/jimp');
}

module.exports = {
  head: {
    titleTemplate: '%s | danhawkes.co.uk',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'danhawkes.co.uk' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },
  css: [
    { src: '~/assets/styles/main.sass', lang: 'sass' },
    { src: '~/assets/styles/highlight.sass', lang: 'sass' }
  ],
  loading: {
    color: '#d0aa00',
    failedColor: '#da1111',
    height: '2px'
  },
  modules: ['nuxtent'],
  plugins: [{ src: '~/plugins/index.js', ssr: false }],
  serverMiddleware: cache ? ['~/api/cache'] : [],
  build: {
    vendor: ['axios', 'vue-clazy-load', 'vue-agile'],
    postcss: false,
    extend: (config, { isClient }) => {
      // Remove jpg/png from url-loader's test so it's handled by responsive-loader.
      const urlLoader = config.module.rules.find(
        rule => rule.loader === 'url-loader'
      );
      urlLoader.test = /\.(gif|svg)$/;

      config.module.rules.push({
        test: /\/software\/.*\.(jpe?g|png)$/,
        loader: 'responsive-loader',
        options: {
          placeholder: true,
          placeholderSize: 10,
          sizes: [262, 524],
          adapter: responsiveLoaderAdapter
        }
      });
      config.module.rules.push({
        test: /\/blogposts\/.*\.(jpe?g|png)$/,
        loader: 'responsive-loader',
        options: {
          placeholder: true,
          placeholderSize: 20,
          sizes: [600, 1200],
          adapter: responsiveLoaderAdapter
        }
      });
    },
    watch: ['~/content/**/*', '~/api/cache.js']
  },
  render: { http2: { push: true } }
};
