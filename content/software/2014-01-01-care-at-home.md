---
title: "Care@Home"
key: "care-at-home"
summary: "Preventative care for outpatients"
platforms:
  - "android"
screenshots: 2
---

Demonstration app for preventative care of outpatients.

* Record meals, activity and upload to server for analysis
* Take pulse and blood oxygen readings via a Bluetooth-connected pulse oximeter.
