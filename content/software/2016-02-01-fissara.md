---
title: Fissara
key: fissara
summary: Workforce scheduling and management
platforms:
  - android
  - ios
description: Workforce scheduling and management
links:
  google_id: uk.co.spaggetti.fissara
screenshots: 11
---

Mobile app for the Fissara system. Comprises a large number of features:

- Scheduling and on-demand job creation
- Asset and site management
- Fault/issue tracking
- Timesheets
- Form data collection
- Document management
- Lone worker safety monitoring
