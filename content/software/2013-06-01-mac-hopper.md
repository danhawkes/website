---
title: "MAC Hopper"
key: "mac-hopper"
summary: "Privacy tool to interfere with passive WiFi tracking"
platforms:
  - "android"
screenshots: 2
links:
  source: "https://github.com/danhawkes/mac-hopper"
---

While you're walking around with your WiFi-enabled phone, it's periodically
searching for networks to connect to.

A number of advertisers, businesses and analytics companies have realised the
value in tracking people using this information.

MAC Hopper periodically randomises the device's WiFi MAC address to interfere
with such tracking; Apple later adopted a similar strategy in iOS 8.

Further information is available in the [project summary][1].

[1]: https://github.com/danhawkes/mac-hopper/blob/master/README.md
