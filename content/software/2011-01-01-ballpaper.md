---
title: "BallPaper"
key: "ballpaper"
summary: "Live wallpaper for Android"
platforms:
  - "android"
screenshots: 8
links:
  google_id: hawkes.wallpaper.ball
---

A physics-sim live wallpaper for Android. At its peak, BallPaper had over 25,000
active installations on the Play Store.
