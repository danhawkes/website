---
title: Configure IP Quality-of-Service in SSH
tags:
  - ssh
  - qos
---

I encountered a strange issue when SSH-ing to a machine on my local network.

SSH would connect, authenticate, then hang indefinitely. Yet sending non-interactive commands worked as normal.

I tried to establish whether the problem was my machine or the network:

- Connecting from a docker container on my machine: **hang**
- Connecting via an intermediary server with agent forwarding: **hang**
- Connecting via an intermediary server, no agent: **success!**

Comparing the logs with `-vvv` the only significant different was this:

```diff
-debug3: ssh_packet_set_tos: set IP_TOS 0×08
+debug3: ssh_packet_set_tos: set IP_TOS 0×20
```

For some reason the connection from my host was using `0×08` [throughput](https://github.com/openssh/openssh-portable/blob/20819b962dc1467cd6fad5486a7020c850efdbee/defines.h#L62) whereas the other was using `0×20` [DSCP_CS1\*](https://github.com/openssh/openssh-portable/blob/20819b962dc1467cd6fad5486a7020c850efdbee/defines.h#L88). These parameters are set server-side to optimise TCP for low latency/high throughput dependent on the nature of the connection.

The workaround hack was the one described [here](https://github.com/tangowhisky37/LetsHack/blob/master/howto/SSH-Hangs-On-Launch.md), where the server's configured to use a specific QoS value. I tried `0×00` as described and it worked, though `0×20` as used by the jump server also works.

I'm blaming this on my Sky broadband router auto-updating and breaking QoS support.

> \* DSCP refers to [Differentiated Services Class Selector](https://en.wikipedia.org/wiki/Differentiated_services#Class_Selector), a bit in the IPv4 header.
