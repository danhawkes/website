---
title: 'Setting the name of Bluetooth LE services'
tags:
- bluetooth
- android
- ios
---

In hindsight, it seems obvious, but the trick to giving your newly defined
Bluetooth peripheral a name that'll appear on other devices is to have it
provide a 'generic_access' service with a 'device_name' characteristic.

Specifically, you want a service with the UUID suffix `1800`, and a readable
characteristic with suffix `2A00`. The (summary) spec is
[here](https://developer.bluetooth.org/gatt/characteristics/Pages/CharacteristicViewer.aspx?u=org.bluetooth.characteristic.gap.device_name.xml).

_(Incidentally, as far as I can tell, the word 'excluded' relating to
characteristic properties means it's implementation-defined -- it doesn't seem
to be specified in the spec.)_
