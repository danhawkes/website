---
title: 'Salt-ssh: some advantages'
tags:
  - salt
  - config-management
---

When using [salt][1] for config management, the master normally communicates
with its minions via ZeroMQ. This is one of its selling points – that it can
handle thousands of minions where the overhead of establishing SSH sessions
would otherwise be significant.

However, there's another option – _salt-ssh_ – which does as you'd expect and
uses SSH for transport. The docs left me with the impression that salt-ssh was
always a poorer alternative, for lazy devs not willing to install the
salt-minion client on their machines. However, it does have several advantages
for small-scale deployments:

## Initial set up

Adding a minion to a cluster with salt ZeroMQ involves several steps:

* Install the `salt-minion` package
* Configure the minion with the master's location
* Start the minion, wait for it to contact with the master, then accept its key
  on the master.

Note it's also possible to pre-seed minions with keys, but that still requires a
bootstrap process (likely involving SSH anyway).

With salt-ssh, minions are simply declared in a roster file on the master:

```yaml
minion-1: 1.1.1.1
minion-2: 1.1.1.2
minion-3: 1.1.1.3
```

Providing the master already has SSH access, no further set up is required.

## Keeping minion versions in sync

Anyone who's attempted to update the salt-minion package via salt will know it's
[not as straight-forward as it could be][2]. With salt-ssh, there's no minion to
install, so updating your cluster is as simple as updating the master.

## Key management

Salt with ZeroMQ ties the minion's key to its ID, so changing the ID means
updating the key on the master. If you've gone down the [recommended route][3]
of embedding metadata in your IDs such as `vm-web-1`, you're in for a lot of
key-churn when reassigning minion roles.

_e.g. a pillar file from the docs:_

```yaml
base:
  '*':
    - packages
  'web*':
    - apache
    - vim
```

With salt-ssh, this is avoided because key management's delegated to SSH, where
you're likely to tie identities to IPs, allowing the minion ID to be dynamic.

## You don't need a permanently-running master

With salt-ssh, the master initiates the connection with the minion, rather than
the other way around. For developers running personal projects from their
laptops, this offers the advantage that you don't need to have a separate
machine running a master just to control a few VMs.

Of course, this does come with some downsides: you can't have the master run
jobs periodically, or in response to external triggers.

[1]: https://docs.saltstack.com
[2]: https://docs.saltstack.com/en/latest/faq.html#what-is-the-best-way-to-restart-a-salt-minion-daemon-using-salt-after-upgrade
[3]: https://docs.saltstack.com/en/latest/topics/best_practices.html
