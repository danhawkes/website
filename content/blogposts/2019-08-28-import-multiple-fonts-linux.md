---
title: 'Install fonts in linux'
tags:
  - linux
  - fonts
---

Given a load of fonts:

```bash
iosevka-bolditalic.ttf
iosevka-boldoblique.ttf
iosevka-bold.ttf
iosevka-extrabolditalic.ttf
iosevka-extraboldoblique.ttf
iosevka-extrabold.ttf
iosevka-extralightitalic.ttf
iosevka-extralightoblique.ttf
```

Move them to the user font directory:

```bash
 mv *.ttf ~/.local/share/fonts,
```

Update the font cache:

```bash
fc-cache -f
```
