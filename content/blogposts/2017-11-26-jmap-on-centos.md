---
title: Getting heap dumps from the openJDK on CentOS
tags:
- java
- centos
---

The built-in way to get a heap dump is via the `jmap` utility. It's part of the
`openjdk-devel` package on CentOS.

If you're working with java 8, you're probably using the `java-1.8.0-openjdk`
package, in which case you'll want `java-1.8.0-openjdk-devel`.

Be careful not to install the `-devel-debug` package, as that contains
debuggable binaries and is therefore much slower.

The basic format for creating a heap dump is:

```sh
jmap -dump:format=b,file=/tmp/dump.hprof <PID>
```

However, there's a few details and error messages to be aware of:

## Jmap should run be under the same user and group as the target JVM

This can be circumvented using the `-F` force flag, but results in a heap dumps
taking a very long time to capture. See this [stackoverflow answer][1] for the
reason why.

To find the user/group required, check `/tmp/hsperfdata_$USER/$PID` for the
relevant PID.

If running under the wrong group user/group, you may see a _"well-known file is
not secure"_ error.

For example, if the JVM's running under the user/group `tomcat`:

```sh
su -g tomcat -c 'jmap -dump:format=b,file=heap.hprof <PID>' tomcat
```

## The debuginfo package may be required

```java
java.lang.InternalError: Metadata does not appear to be polymorphic
```

This unusually interesting error means you need the `-debuginfo` package.

For CentOS, it's in a separate repository as described here:
<https://wiki.centos.org/AdditionalResources/Repositories>.

[1]: https://stackoverflow.com/questions/26140182/running-jmap-getting-unable-to-open-socket-file/35963059#35963059
