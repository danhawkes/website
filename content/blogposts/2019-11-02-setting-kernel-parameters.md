---
title: 'Set kernel parameters via GRUB'
tags:
  - kernel
  - grub
---

Update the master config at `/etc/default/grub`:

```sh
// Disable intel meltdown/spectre mitigations
GRUB_CMDLINE_LINUX_DEFAULT="mitigations=off"
```

Then run `update-grub`, which updates the config used during boot:

```sh
set -e
exec grub-mkconfig -o /boot/grub/grub.cfg "$@"
```

To check the parameters are active, read `/proc/cmdline`:

```
$ cat /proc/cmdline
BOOT_IMAGE=/vmlinuz-5.3.0-21-generic … mitigations=off …
```
