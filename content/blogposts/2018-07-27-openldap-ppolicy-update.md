---
title: 'OpenLDAP upgrade failure: pwdMaxRecordedFailure'
tags:
  - openldap
  - centos
---

On CentOS, updating OpenLDAP from `2.4.44-13.el7` to `2.4.44-15.el7_5` caused us a failure because the ppolicy schema has changed:

```
Checking configuration file failed:
5b5738e8 User Schema load failed for attribute "pwdMaxRecordedFailure". Error code 17: attribute type undefined
5b5738e8 config error processing olcOverlay={0}ppolicy,olcDatabase={1}bdb,cn=config: User Schema load failed for attribute "pwdMaxRecordedFailure". Error code 17: attribute type undefined
slaptest: bad configuration file!
```

The fix is to manually add the missing attribute via [this script](https://bugs.centos.org/file_download.php?file_id=21740&type=bug) (inlined below in case it disappears).

```bash
get_dn () {
    ldapsearch -Y EXTERNAL -H ldapi:/// -b cn=schema,cn=config 2>/dev/null \
    | grep -e '^dn: *cn=[{].*[}]ppolicy,cn=schema,cn=config$'
}

gen_ldif () {
cat << EOF
$1
add: olcAttributeTypes
olcAttributeTypes: {16}( 1.3.6.1.4.1.42.2.27.8.1.30 NAME 'pwdMaxRecordedFailur
 e' EQUALITY integerMatch ORDERING integerOrderingMatch  SYNTAX 1.3.6.1.4.1.
 1466.115.121.1.27 SINGLE-VALUE )
EOF
}

DN=`get_dn`
gen_ldif "$DN" | ldapmodify -Y EXTERNAL -H ldapi:///
```

Ideally, this needs to be applied before the upgrade, but if your LDAP's already down you need to downgrade, run the script, then upgrade again.

```bash
yum downgrade openldap-servers openldap-clients openldap-devel openldap
```
