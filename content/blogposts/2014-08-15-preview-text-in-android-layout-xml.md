---
title: 'Preview text in Android layout XML'
tags:
- android
---

When designing layouts in XML, it's useful to be able to work with a preview
containing some sample text.

Rather than hard-coding a load of text in the layout (which is bad because it
slows down view inflation), you can use the `tools` namespace to achieve the
same effect without the runtime cost.

Instead of this:

```xml
 <TextView
   xmlns:android="http://schemas.android.com/apk/res/android"
   android:text="Lorem Ipsum"/>
```

…do this!

```xml
 <TextView
   xmlns:android="http://schemas.android.com/apk/res/android"
   xmlns:tools="http://schemas.android.com/tools"
   tools:text="Lorem Ipsum"/>
```
