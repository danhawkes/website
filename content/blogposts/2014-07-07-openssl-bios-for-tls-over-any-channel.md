---
title: 'OpenSSL BIOs: TLS over any channel'
tags:
- openssl
- bio
- tls
- ssl
---

Once you come to terms with the complete lack of documentation\*, OpenSSL
actually has some nice API features to discover, like BIOs.

BIOs are an abstraction for I/O, and are used throughout OpenSSL to describe
streams of data within processes. They're pretty cool, because they:

* Allow you to send TLS data over arbitrary channels. They also support
  non-blocking IO, so you can do your TLS handshake via carrier pigeon and
  OpenSSL won't know the difference.
* Can be chained together, so you can do transparent logging of data for
  debugging either side of the encryption.
* Can operate with in-memory buffers (see `BIO_new_pair`), so you can simulate a
  loopback connection without messy file descriptors and the like.

> \* I highly recommend 'the forgotten manual' for this -- _Network Security
> with OpenSSL_
