---
title: Automating interactive prompts with expect
tags:
  - expect
  - automation
---

[Expect](http://expect.sourceforge.net/) is a powerful tool for automating
interactive applications.

It runs script files written in the esoteric "_Tool Command Language_", TCL.

The docs are here:

* [TCL reference](http://tmml.sourceforge.net/doc/tcl/category-index.html)
* [Man page](https://www.tcl.tk/man/expect5.31/expect.1.html)

To invoke expect you simply point it at your script file:

```bash
expect -f <script> [any other args]
```

## Handle a known sequence of prompts

```tcl
spawn annoying_program_with_unskippable_prompt
expect "Is this OK?"
send "yes"
expect "Are you sure?"
send "YES!"
```

## Handle an unknown sequence of prompts

```tcl
expect {
    "Say yes:" {
        send "yes"
        exp_continue
    }
    "Say no:" {
        send "no"
        exp_continue
    }
}
```

`exp_continue` resumes execution, so this is effectively an infinite loop.

## Exit when the spawned command ends

```tcl
expect eof
exit
```

## Parameterise the script with shell arguments

`argv` and `argc` are special variables that hold the values and number of
arguments:

```sh
expect <script> a -b -c -d
…
argc = ["a", "-b", "-c", "-d"]
argv = 4
```

If you want to use these in a `spawn` command, they can be extracted like so:

```tcl
spawn [lindex $argv 0] {*}[lrange $argv 1 end]
```

This would execute:

```
spawn a -b -c -d
```
