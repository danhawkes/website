import Vue from 'vue';

import VueAgile from 'vue-agile';
Vue.use(VueAgile);

import VueClazyLoad from 'vue-clazy-load';
Vue.use(VueClazyLoad);
