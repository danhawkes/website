FROM node:12 as base
WORKDIR /app

FROM base as npm
COPY package.json package-lock.json /app/
RUN npm install --production

FROM base as build
COPY --from=npm /app/node_modules /app/node_modules
COPY . /app
ENV PORT=80
RUN npm run build

FROM base as run
COPY --from=build /app /app
ENV PORT=80
ENV HOST="0.0.0.0"
ENV CACHE_ENABLE=1
EXPOSE 80
CMD ["npm", "run", "start"]
