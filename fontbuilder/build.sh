#!/bin/sh -e


copy_dir="../assets/fonts"
src_dir="fonts"
dst_dir="output"

rm -rf "$dst_dir"
mkdir "$dst_dir"

docker build -t fontconverter .
docker run -i -t --rm -v "$(pwd)/fonts:/fonts:ro" -v "$(pwd)/output:/output" fontconverter

# Copy generated fonts into assets
rm -rf "$copy_dir"
mv "$dst_dir" "$copy_dir/"
