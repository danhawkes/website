#!/usr/bin/env python3

import os
import glob
import re
from subprocess import check_call

src_dir = "fonts"
dst_dir = "output"

files = glob.glob("{}/**/*.*tf".format(src_dir))
for src in files:
    for extension in ['woff', 'woff2']:
        dst = src.replace(src_dir, dst_dir)
        dst = re.sub(r"\.\w+$", '.' + extension, dst)
        dst = dst.replace(' ', '_')
        print('{} -> {}'.format(src, dst))

        os.makedirs(os.path.dirname(dst), exist_ok=True)

        command = ['pyftsubset', src]
        command.append('--output-file={}'.format(dst))

        # 20-7E: Basic latin https://unicode-table.com/en/blocks/basic-latin/
        # 2013-2014: En, Em dash
        # 2018-2019: Left, right single quotes
        # 201C-201D: Left, right double quotes
        # 2022: bullet point
        # 2026: ellipsis
        command.append(
            '--unicodes=20-7E,2013-2014,2018-2019,201C-201D,2022,2026')

        command.append('--flavor={}'.format(extension))
        check_call(command)
