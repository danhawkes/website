#!/bin/sh
set -e

# Fix ownership of output files
finish() {
    user_id=$(stat -c '%u:%g' /output)
    chown -R "$user_id" /output
}
trap finish EXIT

python3 /run.py